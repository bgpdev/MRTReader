package mrt.type.TABLE_DUMP_V2;


import mrt.MRTHeader;
import mrt.MRTRecord;
import network.entities.Prefix;

import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A RIB_AFI object represents the collections of paths for a given prefix.
 */
public class RIB_AFI extends MRTRecord
{
    /** The sequence number of this RIB_AFI object. */
    public final int sequencenumber;

    /** The prefix to which the paths belong. */
    public final Prefix prefix;

    /** A set of RIB_ENTRY objects that represent the different paths. */
    public ArrayList<RIB_ENTRY> entries = new ArrayList<>();

    /**
     * Represents a path for the given prefix.
     */
    public class RIB_ENTRY
    {
        /** The peer from which this path was received. */
        public final PEER_INDEX_TABLE.Peer peer;

        /** The time at which this path was received. */
        public final int originatedtime;

        /** The BGP Path Attributes. */
        public final byte[] attributes;

        /**
         * Constructs a RIB_ENTRY object from a DataInputStream.
         * @param peers A list of peers which is used to convert a Peer index to a Peer object.
         * @param stream The DataInputStream from which this RIB_ENTRY should be constructed.
         * @throws Exception Any exceptions that occur while constructing the RIB_ENTRY.
         */
        RIB_ENTRY(List<PEER_INDEX_TABLE.Peer> peers, DataInputStream stream) throws Exception
        {
            this.peer = peers.get(stream.readUnsignedShort());
            this.originatedtime = stream.readInt();
            int attributeLength = stream.readUnsignedShort();

            attributes = new byte[attributeLength];
            stream.readFully(attributes);
        }

        /**
         * Prints out a RIB_ENTRY object.
         */
        public void print()
        {
            peer.print();
            System.out.println("Originated Time: " + originatedtime);
        }
    }

    /**
     * Construct a RIB_AFI object.
     * @param header The MRTHeader of this MRTRecord.
     * @param peers A list of peers used to convert Peer indexes to Peer objects.
     * @param stream The DataInputStream from which this RIB_AFI should be constructed.
     * @throws Exception Any exceptions that occur while constructing the RIB_AFI.
     */
    public RIB_AFI(MRTHeader header, List<PEER_INDEX_TABLE.Peer> peers, DataInputStream stream) throws Exception
    {
        super(header);

        this.sequencenumber = stream.readInt();

        if(header.subtype == 2 || header.subtype == 3)
            this.prefix = new Prefix(stream, Prefix.Type.IPv4);
        else
            this.prefix = new Prefix(stream, Prefix.Type.IPv6);

        int entrycount = stream.readUnsignedShort();
        for(int i = 0; i < entrycount; i++)
            entries.add(new RIB_ENTRY(peers, stream));
    }

    /**
     * Prints out a RIB_AFI object.
     */
    public void print()
    {
        System.out.println("Sequence Number: " + sequencenumber);

        prefix.print();

        for(RIB_ENTRY entry : entries)
            entry.print();
    }
}
