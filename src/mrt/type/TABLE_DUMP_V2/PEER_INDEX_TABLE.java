package mrt.type.TABLE_DUMP_V2;

import mrt.MRTHeader;
import mrt.MRTRecord;

import java.io.DataInputStream;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of peers which are referenced by RIB_AFI messages.
 */
public class PEER_INDEX_TABLE extends MRTRecord
{
    /**
     * Each instance of the Peer class represents an entry in the PEER_INDEX_TABLE.
     */
    public class Peer
    {
        /** Flags containing the ASN size (16 or 32 bits) and the IP address family (v4 or v6). */
        public final byte type;

        /** The BGP ID of the peer. */
        public final int ID;

        /** The IP address of the peer. Can be either IPv4 or IPv6. */
        public final InetAddress address;

        /** The ASN number of the peer. */
        public final int ASN;

        /**
         * Constructs a peer entry from a DataInputStream.
         * @param stream The DataInputStream from which a peer entry should be constructed.
         * @throws Exception Any exceptions that occur while reading from the stream.
         */
        public Peer(DataInputStream stream) throws Exception
        {
            this.type = stream.readByte();
            this.ID = stream.readInt();

            if((this.type & 1) == 0)
            {
                byte[] buffer = new byte[4];
                stream.readFully(buffer);
                address = InetAddress.getByAddress(buffer);
            }
            else
            {
                byte[] buffer = new byte[16];
                stream.readFully(buffer);
                address = InetAddress.getByAddress(buffer);
            }

            if((this.type & (1 << 1)) == 0)
                ASN = stream.readShort();
            else
                ASN = stream.readInt();
        }

        public void print()
        {
            System.out.println("mrt.type: " + type);
            System.out.println("ID: " + ID);
            System.out.println("IP address: " + address.toString());
            System.out.println("Autonomous System Number: " + ASN);
        }
    }

    /** The BGP Collector ID */
    public final int collector;

    /** The BGP view name. */
    public final byte[] viewname;

    /** The list of peers in this table. */
    public List<Peer> peers = new ArrayList<>();

    /**
     * Constructs a PEER_INDEX_TABLE from a DataInputStream.
     * @param header The associated MRTHeader.
     * @param stream The DataInputStream from which a PEER_INDEX_TABLE should be constructed.
     * @throws Exception Any exceptions that occur while reading from the stream.
     */
    public PEER_INDEX_TABLE(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        this.collector = stream.readInt();
        int viewnamelength = stream.readShort();

        if(viewnamelength != 0)
        {
            this.viewname = new byte[viewnamelength];
            stream.readFully(viewname);
        }
        else
            this.viewname = null;

        int peercount = stream.readShort();
        for(int i = 0; i < peercount; i++)
            peers.add(new Peer(stream));
    }

    /**
     * Prints out the Collector ID, Viewname and the details of each peer.
     */
    public void print()
    {
        System.out.println("Collector ID: " + collector);
        System.out.println("Viewname: " + new String(viewname, StandardCharsets.US_ASCII));
        for(Peer peer : peers)
            peer.print();
    }
}