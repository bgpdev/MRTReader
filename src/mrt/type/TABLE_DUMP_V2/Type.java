package mrt.type.TABLE_DUMP_V2;

/**
 * An enumeration of the subtypes of the TABLE_DUMP_V2 record type.
 */
public enum Type
{
    PEER_INDEX_TABLE(1),
    RIB_IPV4_UNICAST(2),
    RIB_IPV4_MULTICAST(3),
    RIB_IPV6_UNICAST(4),
    RIB_IPV6_MULTICAST(5),
    RIB_GENERIC(6);

    /** The numeric value of the subtype. */
    private final int ID;

    /**
     * Constructs a Type
     * @param value The numeric value of the subtype.
     */
    Type(int value)
    {
        this.ID = value;
    }

    /**
     * Constructs a Type from a numeric value.
     * @param ID The numeric value that should be converted.
     * @return The enumeration value matching this numeric value.
     */
    public static Type fromInt(int ID) throws IllegalArgumentException
    {
        for (Type type : Type.values())
            if (type.ID == ID) return type;
        throw new IllegalArgumentException("Illegal TABLE_DUMP_V2 subtype.");
    }
}
