package mrt.type;

import mrt.MRTHeader;
import mrt.MRTRecord;

import java.io.DataInputStream;
import java.net.Inet4Address;

public class OSPFv2 extends MRTRecord
{
    /** Contains the source IP of the IPv4 header. **/
    public final Inet4Address remote;

    /** Contains the destination IP of the IPv4 header. **/
    public final Inet4Address local;

    /** Contains the message field. (The OSPFv2 packet) **/
    public final byte[] message;

    /**
     * Constructs an OSPFv2 MRTRecord.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream from which the MRTRecord should be constructed.
     * @throws Exception Any exceptions that might occur while parsing.
     */
    public OSPFv2(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        byte[] buffer = new byte[4];
        stream.readFully(buffer);
        remote = (Inet4Address) Inet4Address.getByAddress(buffer);

        stream.readFully(buffer);
        local = (Inet4Address) Inet4Address.getByAddress(buffer);

        this.message = new byte[header.length - 8];
        stream.readFully(message);
    }

    /**
     * Prints out the OSPFv2 record. Currently unimplemented.
     */
    public void print()
    {
        System.out.println("OSPFv2::print is not implemented yet.");
    }
}
