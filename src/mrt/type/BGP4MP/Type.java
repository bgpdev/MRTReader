package mrt.type.BGP4MP;

/**
 * The subtypes of the BGP4MP message type.
 */
public enum Type
{
    BGP4MP_STATE_CHANGE(0),
    BGP4MP_MESSAGE(1),
    BGP4MP_MESSAGE_AS4(4),
    BGP4MP_STATE_CHANGE_AS4(5),
    BGP4MP_MESSAGE_LOCAL(6),
    BGP4MP_MESSAGE_AS4_LOCAL(7);

    /** The numeric value of the subtype. */
    private final int ID;

    /**
     * Constructs a Type
     * @param value The numeric value of the subtype.
     */
    Type(int value)
    {
        this.ID = value;
    }

    /**
     * Constructs a Type from a numeric value.
     * @param ID The numeric value that should be converted.
     * @return The enumeration value matching this numeric value.
     */
    public static Type fromInt(int ID) throws IllegalArgumentException
    {
        for (Type type : Type.values())
            if (type.ID == ID) return type;
        throw new IllegalArgumentException("Illegal BGP4MP subtype.");
    }
}
