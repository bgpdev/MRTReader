package mrt.type.BGP4MP;

import mrt.MRTRecord;
import mrt.MRTHeader;

import java.io.DataInputStream;
import java.net.InetAddress;

/**
 * A STATE_CHANGE MRTRecord represents a change in state with a peering connection.
 */
public class STATE_CHANGE extends MRTRecord
{
    /** The peer ASN from which this message was received. */
    public final int peerASN;

    /** The local ASN of the receiving AS. */
    public final int ASN;

    /** The interface index, may be zero. */
    public final short interface_index;

    /** The address family of this peering connection, either IPv4 or IPv6. */
    public final short address_family;

    /** The IP address of the peer. */
    public final InetAddress peer;

    /** The IP address of the local interface. */
    public final InetAddress local;

    /** The old state of the peering connection. */
    public final short oldstate;

    /** The new state of the peering connection. */
    public final short newstate;

    /**
     * Constructs a STATE_CHANGE object from a DataInputStream.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream from which this STATE_CHANGE should be constructed.
     * @throws Exception Any exceptions that occur while constructing the STATE_CHANGE.
     */
    public STATE_CHANGE(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        if(header.subtype == 0)
        {
            peerASN = stream.readShort();
            ASN = stream.readShort();
        }
        else
        {
            peerASN = stream.readInt();
            ASN = stream.readInt();
        }
        interface_index = stream.readShort();
        address_family = stream.readShort();

        if(address_family == 1)
        {
            byte[] peer_buffer = new byte[4];
            stream.readFully(peer_buffer);
            peer = InetAddress.getByAddress(peer_buffer);

            byte[] local_buffer = new byte[4];
            stream.readFully(local_buffer);
            local = InetAddress.getByAddress(local_buffer);
        }
        else
        {
            byte[] peer_buffer = new byte[16];
            stream.readFully(peer_buffer);
            peer = InetAddress.getByAddress(peer_buffer);

            byte[] local_buffer = new byte[16];
            stream.readFully(local_buffer);
            local = InetAddress.getByAddress(local_buffer);
        }

        oldstate = stream.readShort();
        newstate = stream.readShort();
    }


    /**
     * Prints out the STATE_CHANGE object.
     */
    public void print()
    {
        System.out.println("Peer ASN: " + peerASN);
        System.out.println("Local ASN: " + ASN);
        System.out.println("Interface Index: " + interface_index);
        System.out.println("Address Family: " + address_family);
        System.out.println("Peer IP: " + peer.toString());
        System.out.println("Local IP: " + local.toString());
        System.out.println("Old State: " + oldstate);
        System.out.println("New State: " + newstate);
    }
}
