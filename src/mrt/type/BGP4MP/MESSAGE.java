package mrt.type.BGP4MP;

import mrt.MRTRecord;
import mrt.MRTHeader;

import java.io.DataInputStream;
import java.net.InetAddress;

/**
 * Represents a raw BGP message
 */
public class MESSAGE extends MRTRecord
{
    /** The peer ASN from which this message was received. */
    public int peerASN;

    /** The local ASN of the receiving AS. */
    public int ASN;

    /** The interface index, may be zero. */
    public short interface_index;

    /** The address family of this peering connection, either IPv4 or IPv6. */
    public short address_family;

    /** The IP address of the peer. */
    public InetAddress peer;

    /** The IP address of the local interface. */
    public InetAddress local;

    /** The BGP message that was received. */
    public byte[] message;

    /**
     * Constructs a MESSAGE object from a DataInputStream.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream from which this MESSAGE should be constructed.
     * @throws Exception Any exceptions that occur while constructing the MESSAGE.
     */
    public MESSAGE(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        if(header.subtype == 1 || header.subtype == 6)
        {
            peerASN = stream.readShort();
            ASN = stream.readShort();
        }
        else
        {
            peerASN = stream.readInt();
            ASN = stream.readInt();
        }
        interface_index = stream.readShort();
        address_family = stream.readShort();

        if(address_family == 1)
        {
            {
                byte[] buffer = new byte[4];
                stream.readFully(buffer);
                peer = InetAddress.getByAddress(buffer);
            }
            {
                byte[] buffer = new byte[4];
                stream.readFully(buffer);
                local = InetAddress.getByAddress(buffer);
            }
        }
        else
        {
            {
                byte[] buffer = new byte[16];
                stream.readFully(buffer);
                peer = InetAddress.getByAddress(buffer);
            }
            {
                byte[] buffer = new byte[16];
                stream.readFully(buffer);
                local = InetAddress.getByAddress(buffer);
            }
        }

        if(header.subtype == 1 || header.subtype == 6)
            message = new byte[header.length - (8 + peer.getAddress().length*2)];
        else
            message = new byte[header.length - (12 + peer.getAddress().length*2)];

        stream.readFully(message);
    }

    /**
     * Prints out the MESSAGE object.
     */
    public void print()
    {
        System.out.println("Peer ASN: " + peerASN);
        System.out.println("Local ASN: " + ASN);
        System.out.println("Interface Index: " + interface_index);
        System.out.println("Address Family: " + address_family);
        System.out.println("Peer IP: " + peer.toString());
        System.out.println("Local IP: " + local.toString());
    }
}
