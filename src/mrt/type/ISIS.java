package mrt.type;

import mrt.MRTHeader;
import mrt.MRTRecord;

import java.io.DataInputStream;
import java.net.InetAddress;

/**
 * The ISIS record type. Used to store raw ISIS messages.
 */
public class ISIS extends MRTRecord
{
    /** Contains the message field. (The ISIS packet) **/
    public final byte[] message;

    /**
     * Constructs an ISIS MRTRecord.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream from which the MRTRecord should be constructed.
     * @throws Exception Any exceptions that might occur while parsing.
     */
    public ISIS(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        this.message = new byte[header.length];
        stream.readFully(message);
    }

    /**
     * Prints out the ISIS record. Currently unimplemented.
     */
    public void print()
    {
        System.out.println("ISIS::print is not implemented yet.");
    }
}
