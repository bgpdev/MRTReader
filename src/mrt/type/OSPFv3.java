package mrt.type;

import mrt.MRTHeader;
import mrt.MRTRecord;

import java.io.DataInputStream;
import java.net.InetAddress;

public class OSPFv3 extends MRTRecord
{
    /** Contains the source IP of the IP header. **/
    public final InetAddress remote;

    /** Contains the destination IP of the IP header. **/
    public final InetAddress local;

    /** Contains the message field. (The OSPFv3 packet) **/
    public final byte[] message;

    /**
     * Constructs an OSPFv3 MRTRecord.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream from which the MRTRecord should be constructed.
     * @throws Exception Any exceptions that might occur while parsing.
     */
    public OSPFv3(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        int type = stream.readUnsignedShort();
        byte[] buffer;
        if(type == 1)
            buffer = new byte[4];
        else if(type == 2)
            buffer = new byte[16];
        else
            throw new IllegalArgumentException("Illigal address family: " + type);

        stream.readFully(buffer);
        remote = InetAddress.getByAddress(buffer);
        stream.readFully(buffer);
        local = InetAddress.getByAddress(buffer);

        this.message = new byte[header.length - (buffer.length*2 + 2)];
        stream.readFully(message);
    }

    /**
     * Prints out the OSPFv3 record. Currently unimplemented.
     */
    public void print()
    {
        System.out.println("OSPFv3::print is not implemented yet.");
    }
}
