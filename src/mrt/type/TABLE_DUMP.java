package mrt.type;

import mrt.MRTRecord;
import mrt.MRTHeader;
import network.AFI;

import java.io.DataInputStream;
import java.net.InetAddress;

/**
 * @author Christian Veenman
 * TODO:
 * - Create enum for subtypes AFI_IPv4 and AFI_IPv6
 * - Should be tested thoroughly.
 * - Should include multiple constructors.
 * - Comments should be revisited.
 * - BGPMessage Attributes should be parsed.
 */
public class TABLE_DUMP extends MRTRecord
{
    /** The view number is intended for cases with multiple RIB views. **/
    public final short viewnumber;

    /** The sequence number is an incremental counter for each RIB entry.
     * Should wrapback to zero on overflow. **/
    public final short sequencenumber;

    /** The prefix of this particular RIB entry. */
    public final InetAddress prefix;

    /** The Prefix Length field indicates the length in bits of the prefix mask for the preceding Prefix field. */
    public final byte prefixLength;

    /**  The Status octet is unused in the TABLE_DUMP mrt.type and SHOULD be set to 1. */
    public final byte status;

    /**  The Status octet is unused in the TABLE_DUMP mrt.type and SHOULD be set to 1. */
    public final int originatedTime;

    /** The peer IP address denotes the peer that it heard this BGPMessage Update from.  */
    public final InetAddress peer;

    /** Peer ASN number from which this BGPMessage Update was learned. */
    public final short ASN;

    /** BGP Path Attribute Length */
    public final short attributeLength;

    /** BGP Path Attributes **/
    public final byte[] attributes;

    /**
     * Constructs a TABLE_DUMP object that holds a RIB entry.
     * @param header The MRTHeader of this MRTRecord.
     * @param stream The DataInputStream used to construct the TABLE_DUMP object.
     * @throws Exception Any exceptions that occur while constructing the TABLE_DUMP object.
     */
    public TABLE_DUMP(MRTHeader header, DataInputStream stream) throws Exception
    {
        super(header);

        this.viewnumber = stream.readShort();
        this.sequencenumber = stream.readShort();

        if(header.subtype == AFI.IPV4.value)
        {
            byte[] buffer = new byte[4];
            stream.readFully(buffer);
            prefix = InetAddress.getByAddress(buffer);
        }
        else if(header.subtype == AFI.IPV6.value)
        {
            byte[] buffer = new byte[16];
            stream.readFully(buffer);
            prefix = InetAddress.getByAddress(buffer);
        }
        else
            throw new Exception("TABLE_DUMP: Header subtype is not supported: " + header.subtype);

        prefixLength = stream.readByte();
        status = stream.readByte();
        originatedTime = stream.readInt();

        if(header.subtype == AFI.IPV4.value)
        {
            byte[] buffer = new byte[4];
            stream.readFully(buffer);
            peer = InetAddress.getByAddress(buffer);
        }
        else
        {
            byte[] buffer = new byte[16];
            stream.readFully(buffer);
            peer = InetAddress.getByAddress(buffer);
        }

        ASN = stream.readShort();
        attributeLength = stream.readShort();

        attributes = new byte[attributeLength];
        stream.read(attributes, 0, attributeLength);
    }

    /**
     * Prints out the TABLE_DUMP record.
     */
    public void print()
    {
        System.out.println("View Number: " + viewnumber);
        System.out.println("Sequence Number: " + sequencenumber);
        System.out.println("Prefix: " + prefix.toString());
        System.out.println("Prefix Length: " + prefixLength);
        System.out.println("Status: " + status);
        System.out.println("Originated Time: " + originatedTime);
        System.out.println("Peer: " + peer.toString());
        System.out.println("Autonomous System Number: " + ASN);
        System.out.println("Attribute Length: " + attributeLength);
    }
}
