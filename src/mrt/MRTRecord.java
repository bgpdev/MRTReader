package mrt;

/**
 * The abstract base class of all MRTRecord types.
 */
public abstract class MRTRecord
{
    /** The header of the MRTRecord */
    public final MRTHeader header;

    /**
     * Constructs a MRTRecord with its associated MRTHeader
     * @param header The MRTHeader of this MRTRecord.
     */
    public MRTRecord(MRTHeader header)
    {
        this.header = header;
    }

    /**
     * Prints out the MRTRecord, all concrete subclasses must implement this.
     */
    public abstract void print();
}
