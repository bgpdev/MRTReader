package mrt;

import java.io.DataInputStream;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Represents the header of each MRTRecord.
 */
public class MRTHeader
{
    /** The timestamp of this MRTRecord in seconds.
     * In case an Extended Timestamp is present this Instant will also hold microseconds. */
    public final Instant timestamp;

    /** The type of this MRTRecord. */
    public final MRTType type;

    /** The subtype of this MRTRecord. */
    public final short subtype;

    /** The length in bytes of this MRTRecord. */
    public final int length;

    /**
     * Constructs a MRTHeader from a DataInputStream.
     * @param stream The DataInputStream from which the MRTHeader is constructed.
     * @throws Exception Any exceptions while reading from the DataInputStream.
     */
    MRTHeader(DataInputStream stream) throws Exception
    {
        Instant timestamp = Instant.ofEpochSecond(stream.readInt());
        this.type = MRTType.resolve(stream.readShort());
        this.subtype = stream.readShort();
        this.length = stream.readInt();

        if(this.type.isExtended())
            this.timestamp = timestamp.plus(stream.readInt(), ChronoUnit.MICROS);
        else
            this.timestamp = timestamp;
    }

    /**
     * Prints out the MRTHeader.
     */
    public void print()
    {
        System.out.println("Timestamp: " + timestamp.toString());
        System.out.println("Type: " + type);
        System.out.println("Subtype: " + subtype);
        System.out.println("Length: " + length);
    }
}
