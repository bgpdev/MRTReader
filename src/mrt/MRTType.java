package mrt;

/**
 * An enum with all supported MRTTypes.
 */
public enum MRTType
{
    NULL(0),
    START(1),
    DIE(2),
    I_AM_DEAD(3),
    PEER_DOWN(4),
    BGP(5),
    RIP(6),
    IDRP(7),
    RIPNG(8),
    BGP4PLUS(9),
    BGP4PLUS_01(10),
    OSPFv2(11),
    TABLE_DUMP(12),
    TABLE_DUMP_V2(13),
    BGP4MP(16),
    BGP4MP_ET(17),
    ISIS(32),
    ISIS_ET(33),
    OSPFv3(48),
    OSPFv3_ET(49);

    /** The numeric value of this MRTType */
    public final int value;

    /**
     * Constructs an MRTType from a value.
     * @param value The integer representing the MRTType.
     */
    MRTType(int value)
    {
        this.value = value;
    }

    /**
     * Converts an integer to a MRTType.
     * @param value The integer that should be converted to a MRTType
     * @return The MRTType that is represented by id.
     * @throws IllegalArgumentException Thrown when the id cannot be converted to an MRTType.
     */
    public static MRTType resolve(int value) throws IllegalArgumentException
    {
        for (MRTType type : MRTType.values())
            if (type.value == value)
                return type;

        throw new IllegalArgumentException("Unknown MRTRecord type " + value);
    }

    /**
     * Checks whether this MRTType should contain an Extended Timestamp in the MRTHeader.
     * @return True when this MRTType should contain an Extended Timestamp.
     */
    public boolean isExtended()
    {
        return value == 49 || value == 33 || value == 17;
    }
}