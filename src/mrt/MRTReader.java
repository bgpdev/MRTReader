package mrt;

import mrt.type.BGP4MP.MESSAGE;
import mrt.type.BGP4MP.STATE_CHANGE;
import mrt.type.ISIS;
import mrt.type.OSPFv2;
import mrt.type.OSPFv3;
import mrt.type.TABLE_DUMP;
import mrt.type.TABLE_DUMP_V2.PEER_INDEX_TABLE;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import mrt.type.TABLE_DUMP_V2.Type;

import java.io.DataInputStream;
import java.io.InputStream;


/**
 * The MRTReader class can read a variety of MRT records from an MRT formatted file.
 * It parses MRT formatted files compliant with RFC6369: https://tools.ietf.org/html/rfc6396
 */
public class MRTReader
{
    /** The DataInputStream that will be used to read MRT records. */
    private DataInputStream stream;

    /** The PEER_INDEX_TABLE that will be used to specify peers in RIB_AFI entries. */
    private PEER_INDEX_TABLE peers;

    /**
     * Constructs an MRTReader from an InputStream
     * @param stream The InputStream from which we will read MRT records.
     */
    public MRTReader(InputStream stream)
    {
        assert(stream != null);
        this.stream = new DataInputStream(stream);
    }

    /**
     * Reads an MRT record from the stream.
     * @return The next MRT record in the stream.
     * @throws Exception Exceptions that occur during the parsing.
     */
    public MRTRecord read() throws Exception
    {
        // Construct the MRTHeader.
        MRTHeader header = new MRTHeader(stream);

        // Parse the MRT record specified by the headers type.
        switch (header.type)
        {
            case NULL:
                throw new Exception("[Deprecated NULL] This operation does not do anything.");
            case START:
                throw new Exception("[Deprecated START] Collector is about to begin generating MRT records.");
            case DIE:
                throw new Exception("[Deprecated DIE] Remote MRT Repository should stop accepting messages.");
            case I_AM_DEAD:
                throw new Exception("[Deprecated I_AM_DEAD] Collector has shutdown and has stopped generating MRT records.");
            case PEER_DOWN:
                throw new Exception("[Deprecated PEER_DOWN] A connection with a peer was lost.");
            case BGP:
                throw new Exception("[Deprecated BGPMessage]");
            case RIP:
                throw new Exception("[Deprecated RIP]");
            case IDRP:
                throw new Exception("[Deprecated IDRP]");
            case RIPNG:
                throw new Exception("[Deprecated RIPNG]");
            case BGP4PLUS:
                throw new Exception("[Deprecated BGP4PLUS]");
            case BGP4PLUS_01:
                throw new Exception("[Deprecated BGP4PLUS_01]");
            case TABLE_DUMP:
                return new TABLE_DUMP(header, stream);
            case TABLE_DUMP_V2:
                switch (Type.fromInt(header.subtype))
                {
                    case PEER_INDEX_TABLE:
                        this.peers = new PEER_INDEX_TABLE(header, stream);
                        return this.peers;
                    case RIB_IPV4_UNICAST:
                        return new RIB_AFI(header, peers.peers, stream);
                    case RIB_IPV4_MULTICAST:
                        return new RIB_AFI(header, peers.peers, stream);
                    case RIB_IPV6_UNICAST:
                        return new RIB_AFI(header, peers.peers, stream);
                    case RIB_IPV6_MULTICAST:
                        return new RIB_AFI(header, peers.peers, stream);
                    case RIB_GENERIC:
                        throw new Exception("RIB_GENERIC is not implemented.");
                }
                break;
            case BGP4MP_ET:
            case BGP4MP:
                switch (mrt.type.BGP4MP.Type.fromInt(header.subtype))
                {
                    case BGP4MP_STATE_CHANGE:
                        return new STATE_CHANGE(header, stream);
                    case BGP4MP_MESSAGE:
                        return new MESSAGE(header, stream);
                    case BGP4MP_MESSAGE_AS4:
                        return new MESSAGE(header, stream);
                    case BGP4MP_STATE_CHANGE_AS4:
                        return new STATE_CHANGE(header, stream);
                    case BGP4MP_MESSAGE_LOCAL:
                        return new MESSAGE(header, stream);
                    case BGP4MP_MESSAGE_AS4_LOCAL:
                        return new MESSAGE(header, stream);
                }
                break;
            case ISIS_ET:
            case ISIS:
                return new ISIS(header, stream);
            case OSPFv2:
                return new OSPFv2(header, stream);
            case OSPFv3_ET:
            case OSPFv3:
                return new OSPFv3(header, stream);
        }

        throw new Exception("Error: Unsupported MRT Type " + header.type);
    }
}
