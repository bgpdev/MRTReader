# README

## Introduction
The MRTReader project contains an MRTReader class written in Java that can read MRT records from an InputStream.
Multi-Threaded Routing Toolkit (MRT) is widely used in storing archives of BGP messages.
RIPE RRC's and Routeviews both use this format.

## Example usage:
To create a MRTReader just pass in the InputStream:
```
// Create an MRTReader that reads MRTRecords from an InputStream.
InputStream stream = ...
MRTReader reader = new MRTReader(stream);
```

To read the next MRTRecord from the stream:
```
MRTRecord record = reader.read();

// Often used when parsing BGP Update archives
if(record instanceof BGP4MP.MESSAGE)
   ...
   
// Often used when parsing BGP RIB archives
if(record instanceof RIB_AFI)
   ...
   
// The old way of storing BGP RIB archives
if(record instanceof TABLE_DUMP)
  ...
```

See the mrt.type package for all possible MRTRecord types that are supported.
The MRTReader is built in a modular way, such that the parser of your choice can be used to do subsequent
parsing of BGP, ISIS, OSPFv2 and OSPFv3 messages.